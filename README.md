# three-random-digits-one-through-nine

Generates a string containing three random digits from 1 through 9.

## Usage

```js
const sequence = threeRandomDigitsOneThroughNine()
```

## See Also

- https://twitter.com/simon_swain/status/1044466605181231104
