const { strictEqual } = require('assert')
const threeRandomDigitsOneThroughNine = require('.')

for (let i = 0; i < 1000; i++) {
  const actual = threeRandomDigitsOneThroughNine()
  strictEqual(actual.length, 3)
  strictEqual(actual.includes('0'), false)
}
