const randomDigitOneThroughNine = require('random-digit-one-through-nine')

module.exports = function threeRandomDigitsOneThroughNine () {
  return randomDigitOneThroughNine().toString() +
    randomDigitOneThroughNine().toString() +
    randomDigitOneThroughNine().toString()
}
